if (Drupal.jsEnabled) { // global killswitch
(function($) { // Dollar namespace scoping
Drupal.LiveSearch = {
  timer: null, 
  localizedTerm: null, 
  id: 'live-search-results',
  
  eventListener: function(event) {
    if (event.keyCode == 27) { // KEY_ESC = 27
      $(this).val('');
      // I'm fed up with debugging animations. ITFC if you like.
      $('#' + Drupal.LiveSearch.id).empty().hide();
    }
    else {
      try {window.clearTimeout(Drupal.LiveSearch.timer);} catch (e) {}
      // Throttle and delay
      Drupal.LiveSearch.timer = window.setTimeout(
        'Drupal.LiveSearch.initiateSearch()', 
        parseInt(Drupal.settings.liveSearch.delayDuration) || 1250
      );
    }
  },
  
  initiateSearch: function() {
    var id = this.id;
    // Typing this over and over drives me over the edge.
    var settings = Drupal.settings.liveSearch;
    var type = settings.targetSearchBox || 'theme';
    var searchBox = $('input#edit-search-' + type + '-form-keys');
    var keyword = Drupal.encodeURIComponent(searchBox.val());
    
    if (!keyword ||
        keyword.length < settings.minimumWordSize ||
        keyword == this.localizedTerm) {
      $('#' + id).empty().hide();
      return;
    } else {
      searchBox.addClass('throbbing');
      this.requestAndInject(settings.queryURL + '/' + keyword);
    }
  },
  
  requestAndInject: function(url) {
    var settings = Drupal.settings.liveSearch;
    var type = settings.targetSearchBox || 'theme';
    var id = this.id, scrollTo = this.scrollTo;
    $.getJSON(url, function(data) {
      var target = $('#' + id);
      target.html(data.found ? data.results : data.message).show();
      $('input#edit-search-' + type + '-form-keys').removeClass('throbbing');
      // Handle paging.
      $('a[@href^=' + settings.queryURL + ']', target).click(function(event) {
        Drupal.LiveSearch.requestAndInject($(this).attr('href'));
        return false;
      });
      // Scroll to the top of the search results.
      if (settings.scrollToResults) {
        scrollTo(target.get(0));
      }
    });
  },
  
  scrollTo: function(element) {
    var t = 0, l = 0;
    // Retrieve cumulative offset of the search results element.
    do {
      t += element.offsetTop || 0;
      l += element.offsetLeft || 0;
      element = element.offsetParent;
    } while (element);
    // Scroll to the element.
    window.scrollTo(l, t);
  }
};

$(document).ready(function() {
  var settings = Drupal.settings.liveSearch;
  // User permission killswitch
  if (!settings.searchAllowed) {
    return;
  }
  var type = settings.targetSearchBox || 'auto';
  var searchBox = $('#edit-search-' + type + '-form-keys');
  var searchForm = $('form#search-' + type + '-form');
  if (type == 'auto') {
    searchBox = $('#edit-search-block-form-keys, #edit-search-theme-form-keys, #search-box, #searchbox');
    if (searchBox.length == 0) {
      return;
    }
    searchBox = $(searchBox.get(0));
    searchForm = $(searchBox).parents('form');
  }
  var localizedTerm;
  
  // Search box killswitch, in case a search block is optionally visible.
  if (searchBox.length == 0) {
    return;
  }
  
  Drupal.LiveSearch.localizedTerm = localizedTerm = 
    $('input[@type=submit]', searchForm).val() || "Search";
  
  searchForm.addClass('live-search');
  
  if (!settings.useCustomElement) {
    searchForm.append('<div id="' + Drupal.LiveSearch.id + '"></div>');
  } else {
    Drupal.LiveSearch.id = settings.customElementId;
  }
  var target = $('#' + Drupal.LiveSearch.id).hide();
  
  if (settings.hideSnippets) {
    target.addClass('hide-snippet');
  }
  target.addClass(settings.showItemInfo ? 'show-item-info' : 'hide-item-info');
  
  // Apple HIG fans should appreciate this option.
  if (settings.compactSearchBox) {
    searchForm.addClass('compact');
    searchBox.val(
      localizedTerm
    ).focus(function() {
      if ($(this).val() == localizedTerm) {
        $(this).val('');
      }
    }).blur(function() {
      if (!$(this).val()) {
        $(this).val(localizedTerm);
      }
    });
  }
  
  searchBox.addClass('form-autocomplete').
            keyup(Drupal.LiveSearch.eventListener).
            attr('autocomplete', 'off');
});
})(jQuery); // End dollar namespace scoping
} // End global killswitch